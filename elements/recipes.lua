local S = minetest.get_translator("elements")

-- Material Reducer recipes

technic.register_recipe_type("material_reducing", {
	description = S("Material Reducing"),
	input_size = 1,
	output_size = 6,
})

function elements.register_reducer_recipe(data)
	data.time = data.time or 2
	technic.register_recipe("material_reducing", data)
end

local reducer_recipes = {
	{"default:stone", {"elements:oxygen 48", "elements:silicon 24"}},

	{"default:cobble", {"elements:oxygen 48", "elements:silicon 24"}},

	{"default:leaves", {"elements:hydrogen 18", "elements:oxygen", "elements:carbon 15", "elements:magnesium", "elements:nitrogen"}},

	{"default:desert_stone", {"elements:oxygen 48", "elements:silicon 21", "elements:iron 3"}},

	{"default:gravel", {"elements:oxygen 48", "elements:silicon 24"}},

	{"default:obsidian", {"elements:oxygen 45", "elements:silicon 18", "elements:iron 2", "elements:aluminum 6", "elements:sodium"}},

	{"default:obsidian_shard", {"elements:oxygen 5", "elements:silicon 2"}},

	{"default:sand", {"elements:oxygen 48", "elements:silicon 24"}},

	{"default:desert_sand", {"elements:oxygen 48", "elements:silicon 24", "elements:iron"}},

	{"default:sandstone", {"elements:oxygen 48", "elements:silicon 24"}},

	{"default:sandstone_silver", {"elements:oxygen 48", "elements:silicon 24"}},

	{"default:dirt", {"elements:oxygen 36", "elements:silicon 12", "elements:carbon 18", "elements:hydrogen 6", "elements:aluminum 4", "elements:sodium"}},

	{"default:clay", {"elements:oxygen 38", "elements:hydrogen 18", "elements:silicon 8", "elements:aluminum 8"}},

	{"default:clay_lump", {"elements:oxygen 7", "elements:hydrogen 4", "elements:silicon 2", "elements:aluminum 2"}},

	-- default ores

	{"default:stone_with_coal", {"elements:carbon 28", "elements:oxygen 44", "elements:silicon 22", "elements:hydrogen 6"}},

	{"default:stone_with_copper", {"elements:carbon 52", "elements:oxygen 16", "elements:silicon 8", "elements:silver 2", "elements:titanium 2", "elements:iron 2"}},

	{"default:stone_with_iron", {"elements:iron 52", "elements:oxygen 24", "elements:silicon 12", "elements:copper 4", "elements:cobalt 2"}},

	{"default:stone_with_tin", {"elements:tin 52", "elements:oxygen 24", "elements:silicon 12", "elements:arsenic 4", "elements:zirconium 4", "elements:tantalum"}},

	{"default:stone_with_gold", {"elements:gold 52", "elements:oxygen 12", "elements:silicon 6", "elements:silver 4", "elements:copper 4", "elements:rhodium"}},

	{"default:stone_with_mese", {"elements:oxygen 36", "elements:silicon 12", "elements:beryllium 6", "elements:aluminum 6", "elements:iron 3"}},

	{"default:stone_with_diamond", {"elements:carbon 28", "elements:oxygen 40", "elements:silicon 20", "elements:manganese 6", "elements:titanium 3", "elements:chromium 3"}},

	-- default lumps

	{"default:coal_lump", {"elements:carbon 24"}},

	{"default:copper_lump", {"elements:copper 52", "elements:oxygen 6", "elements:silicon 3", "elements:nickel", "elements:thallium"}},

	{"default:iron_lump", {"elements:iron 52", "elements:oxygen 6", "elements:silicon 3", "elements:nickel 4", "elements:carbon"}},

	{"default:tin_lump", {"elements:tin 52", "elements:oxygen 6", "elements:silicon 3", "elements:iron 4", "elements:zirconium"}},

	{"default:gold_lump", {"elements:iron 52", "elements:oxygen 6", "elements:silicon 3", "elements:silver", "elements:copper"}},

	{"default:mese_crystal", {"elements:oxygen 12", "elements:silicon 4", "elements:beryllium 2", "elements:aluminum 2"}},

	{"default:diamond", {"elements:carbon 24"}},

}

local i
for i = 1, #reducer_recipes do

	elements.register_reducer_recipe({
		input = {reducer_recipes[i][1]},
		output = reducer_recipes[i][2]
	})
end


-- Atomic Constructor recipes

technic.register_recipe_type("atomic_constructing", {
	description = S("Atomic Constructing"),
	input_size = 5,
	output_size = 1,
})

function elements.register_constructor_recipe(data)
	data.time = data.time or 2
	technic.register_recipe("atomic_constructing", data)
end

local constructor_recipes = {
	-- TODO: expand constructor formspec to have 6 input slots
	{{"elements:oxygen 48", "elements:silicon 24"}, "default:sand"},
}

for i = 1, #constructor_recipes do

	elements.register_constructor_recipe({
		input = constructor_recipes[i][1],
		output = constructor_recipes[i][2]
	})
end
