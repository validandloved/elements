elements = {

	modpath = minetest.get_modpath("elements")
}

dofile(elements.modpath .. "/items.lua")
dofile(elements.modpath .. "/recipes.lua")
dofile(elements.modpath .. "/machines.lua")
