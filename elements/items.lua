local S = minetest.get_translator("elements")

local default_stack_max = tonumber(minetest.settings:get("default_stack_max")) or 99
local atom_stack_max = 96 * default_stack_max

-- `name` is the lowercase element name
function elements.register_element(name)

	-- convert first ascii letter in `name` to uppercase, and store
	-- this new string in `Name`
	local Name = string.char(name:byte(1, 1) - 32) .. name:sub(2)

	minetest.register_craftitem("elements:" .. name, {
		description = S(Name),
		inventory_image = "elements_" .. name .. ".png",
		stack_max = atom_stack_max,
	})
end

local elements_list = {

	-- First row
	"hydrogen",
	"helium",

	-- Second row
	"lithium",
	"beryllium",
	"boron",
	"carbon",
	"nitrogen",
	"oxygen",
	"fluorine",
	"neon",

	-- Third row
	"sodium",
	"magnesium",
	"aluminum",
	"silicon",
	"phosphorus",
	"sulfur",
	"chlorine",
	"argon",

	-- Fourth row
	"potassium",
	"calcium",
	"scandium",
	"titanium",
	"vanadium",
	"chromium",
	"manganese",
	"iron",
	"cobalt",
	"nickel",
	"copper",
	"zinc",
	"gallium",
	"germanium",
	"arsenic",
	"selenium",
	"bromine",
	"krypton",

	-- Fifth row
	"rubidium",
	"strontium",
	"yttrium",
	"zirconium",
	"niobium",
	"molybdenum",
	"technetium",
	"ruthenium",
	"rhodium",
	"palladium",
	"silver",
	"cadmium",
	"indium",
	"tin",
	"antimony",
	"tellurium",
	"iodine",
	"xenon",

	-- Sixth row
	"cesium",
	"barium",

	-- Lanthanides
	"lanthanum",
	"cerium",
	"praseodymium",
	"neodymium",
	"promethium",
	"samarium",
	"europium",
	"gadolinium",
	"terbium",
	"dysprosium",
	"holmium",
	"erbium",
	"thulium",
	"ytterbium",
	"lutetium",

	"hafnium",
	"tantalum",
	"tungsten",
	"rhenium",
	"osmium",
	"iridium",
	"platinum",
	"gold",
	"mercury",
	"thallium",
	"lead",
	"bismuth",
	"polonium",
	"astatine",
	"radon",

	-- Seventh row
	"francium",
	"radium",

	-- Actinides
	"actinium",
	"thorium",
	"protactinium",
	"uranium",
	"neptunium",
	"plutonium",
	"americium",
	"curium",
	"berkelium",
	"californium",
	"einsteinium",
	"fermium",
	"mendelevium",
	"nobelium",
	"lawrencium",

	"rutherfordium",
	"dubnium",
	"seaborgium",
	"bohrium",
	"hassium",
	"meitnerium",
	"darmstadtium",
	"roentgenium",
	"copernicium",
	"nihonium",
	"flerovium",
	"moscovium",
	"livermorium",
	"tennessine",
	"oganesson",
}

local i
for i = 1, #elements_list do
	elements.register_element(elements_list[i])
end
