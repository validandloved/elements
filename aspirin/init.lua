-- Aspirin mod for Minetest.
-- Copyright (C) 2024  Olivia May
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

minetest.register_craftitem("aspirin:aspirin", {
	description = "Aspirin",
	inventory_image = "aspirin.png",
	on_use = function(itemstack, user, pointed_thing)

		local hp = user:get_hp()
		if hp >= user:get_properties().hp_max then
			return nil
		end

		-- two hearts
		hp = hp + 4
		user:set_hp(hp)
		itemstack:take_item(1)

		return itemstack
	end,
})

minetest.register_craftitem("aspirin:bottle_of_aspirin", {
	description = "Bottle of Aspirin",
	inventory_image = "bottle_of_aspirin.png",
	on_use = function(itemstack, user, pointed_thing)

		local hp = user:get_hp()
		if hp >= user:get_properties().hp_max then
			return nil
		end

		-- 10 hearts
		hp = hp + 20
		user:set_hp(hp)
		itemstack:take_item(1)

		return itemstack
	end,
})

has_ethereal = minetest.get_modpath("ethereal")
has_moretrees = minetest.get_modpath("moretrees")
has_ebiomes = minetest.get_modpath("ebiomes")
has_everness = minetest.get_modpath("everness")
has_no_willows = not (has_ethereal or has_moretrees or has_ebiomes or has_everness)
has_technic = minetest.get_modpath("technic")

local function register_recipe(leaves)

	if has_technic then
		technic.register_extractor_recipe(
			{input = {leaves},
			output = "aspirin:aspirin"
		})
	else
		minetest.register_craft({
			output = "aspirin:aspirin",
			type = "shapeless",
			recipe = {
				leaves,
			},
		})
	end
end

if has_no_willows then register_recipe("default:leaves") 

else

if has_ethereal then register_recipe("ethereal:willow_twig") end
if has_moretrees then register_recipe("moretrees:willow_leaves") end
if has_ebiomes then register_recipe("ebiomes:willow_leaves") end
if has_everness then register_recipe("everness:willow_leaves") end

end

minetest.register_craft({
	output = "aspirin:bottle_of_aspirin",
	type = "shapeless",
	recipe = {
		"aspirin:aspirin",
		"aspirin:aspirin",
		"aspirin:aspirin",
		"aspirin:aspirin",
		"aspirin:aspirin",
		"vessels:glass_bottle",
	},
})
